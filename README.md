# Code for 620 Research Project
By: James Krepelka

There will be two projects in here.

## TestHelper
Sits on a SUT and accepts inputs from remote RPCs.
Interacts with the database to prepare, test, and
clean after testing.

## Website
The simple user-interface for interacting with the
array of SUTs that were set up.
