// Package client implements a client for the TestHelper service.
package client

import (
	"context"
	"fmt"
	"time"

	pb "../../helper"
	"google.golang.org/grpc"
)

// TODO: Pass in a connection, not an ip.
func HealthCheck(ctx context.Context, ip string) (bool, error) {
	// Connect to the server.
	conn, err := grpc.DialContext(ctx, ip, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return false, fmt.Errorf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewTestHelperClient(conn)

	// Check the server's health.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*4)
	defer cancel()
	r, err := c.HealthCheck(ctx, &pb.HealthCheckRequest{})
	if err != nil {
		return false, fmt.Errorf("could not greet: %v", err)
	}

	return r.Healthy, nil
}
