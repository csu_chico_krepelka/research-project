package query

type Query interface {
	Run() error
}
