package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"

	"../query"

	// Needed for supporting the mysql dbs.
	_ "github.com/go-sql-driver/mysql"
)

// DB represents a mysql DB connection handler.
type DB struct {
	user   string
	pass   string
	dbName string
	conn   *sql.DB
}

// NewConnection returns a new mysql connection handler.
//
// You still need to call Connect() before using.
func NewConnection() *DB {
	return &DB{
		user:   "helper",
		pass:   os.Getenv("DBPASS"),
		dbName: "researchTesting",
	}
}

// Healthy pings the mysql instance to confirm connectivity.
func (db *DB) Healthy() bool {
	if db.conn == nil {
		return false
	}
	if err := db.conn.Ping(); err != nil {
		return false
	}
	return true
}

// Connect prepares a connection to the mysql instance.
func (db *DB) Connect() error {
	s := fmt.Sprintf("%s:%s@/%s", db.user, db.pass, db.dbName)
	conn, err := sql.Open("mysql", s)
	if err != nil {
		return err
	}
	db.conn = conn
	return nil
}

// Close disconnects from the mysql instance.
func (db *DB) Close() {
	if db.conn == nil {
		return
	}
	db.conn.Close()
}

// Setup creates the tables needed for testing.
func (db *DB) Setup() error {
	if db.conn == nil {
		return errors.New("no active connection")
	}
	err := createTables(db.conn)
	if err != nil {
		return err
	}
	return nil
}

// TearDown drops the tables created in Setup.
func (db *DB) TearDown() {
	if db.conn == nil {
		return
	}
	err := dropTables(db.conn)
	if err != nil {
		log.Printf("error(s) when tearing down: %v\n", err)
	}
}

// Fill loads the database up with the given amount of data.
func (db *DB) Fill(people, avgFriends, avgInterests int) error {
	//TODO: average friends cannot be close to people

	if db.conn == nil {
		return errors.New("no active connection")
	}

	rand.Seed(time.Now().UnixNano())

	// Insert People
	err := insertPeople(db.conn, people)
	if err != nil {
		return err
	}

	// Insert Friends
	err = insertFriends(db.conn, people, avgFriends)
	if err != nil {
		return err
	}

	// Insert Interests
	// 10% as many unique interests as people in a group.
	numInterests := people / 10
	err = insertInterests(db.conn, numInterests)
	if err != nil {
		return err
	}

	// Insert Interest Links
	err = insertInterestLinks(db.conn, people, numInterests, avgInterests)
	if err != nil {
		return err
	}

	return nil
}

type mysqlCreateQuery struct {
	db   *DB
	stmt *sql.Stmt
	args [][]interface{}
}

func (m mysqlCreateQuery) Run() error {
	for _, args := range m.args {
		_, err := m.stmt.Exec(args...)
		if err != nil {
			return err
		}
	}
	return nil
}

const insPersonQueryTemplate = "INSERT INTO Person (FirstName, LastName, ProfileImageURL) VALUES (?, ?, ?)"

// CreateQuery returns a prepared query object for running tests with.
//
// qNum specifies which query to prepare.
func (db *DB) CreateQuery(qNum int) (query.Query, error) {
	var args [][]interface{}
	var stmt *sql.Stmt
	var err error
	switch qNum {
	case 1:
		args = insertPeopleArgs(10000)
		stmt, err = db.conn.Prepare(insPersonQueryTemplate)
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("Unrecognized query number")
	}

	q := mysqlCreateQuery{
		db:   db,
		stmt: stmt,
		args: args,
	}
	return q, nil
}

func dropTables(db *sql.DB) []error {
	var errs []error

	_, err := db.Exec(`DROP TABLE Person_has_Interests`)
	if err != nil {
		errs = append(errs, fmt.Errorf("failed to drop the Person_has_Interests table: %v", err))
	}
	_, err = db.Exec(`DROP TABLE Interests`)
	if err != nil {
		errs = append(errs, fmt.Errorf("failed to drop the Interests table: %v", err))
	}
	_, err = db.Exec(`DROP TABLE Friends`)
	if err != nil {
		errs = append(errs, fmt.Errorf("failed to drop the Friends table: %v", err))
	}
	_, err = db.Exec(`DROP TABLE Person`)
	if err != nil {
		errs = append(errs, fmt.Errorf("failed to drop the Person table: %v", err))
	}
	return errs
}

func createTables(db *sql.DB) error {
	_, err := db.Exec(
		`CREATE TABLE Person (
					id INT UNSIGNED NOT NULL AUTO_INCREMENT,
					FirstName VARCHAR(100) NULL,
					LastName VARCHAR(100) NULL,
					ProfileImageURL VARCHAR(2048) NULL,
					Created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
					PRIMARY KEY (id),
					UNIQUE INDEX idPerson_UNIQUE (id ASC))
			`)
	if err != nil {
		return fmt.Errorf("failed to create Person table: %v", err)
	}

	_, err = db.Exec(
		`CREATE TABLE Friends (
					p_id1 INT UNSIGNED NOT NULL,
					p_id2 INT UNSIGNED NOT NULL,
					PRIMARY KEY (p_id1, p_id2),
					INDEX fk_Person_has_Person_Person1_idx (p_id2 ASC),
					INDEX fk_Person_has_Person_Person_idx (p_id1 ASC),
					CONSTRAINT fk_Person_has_Person_Person
							FOREIGN KEY (p_id1)
							REFERENCES Person (id)
							ON DELETE CASCADE
							ON UPDATE NO ACTION,
					CONSTRAINT fk_Person_has_Person_Person1
							FOREIGN KEY (p_id2)
							REFERENCES Person (id)
							ON DELETE CASCADE
							ON UPDATE NO ACTION)
			`)
	if err != nil {
		return fmt.Errorf("failed to create Friends table: %v", err)
	}

	_, err = db.Exec(
		`CREATE TABLE Interests (
					id INT UNSIGNED NOT NULL AUTO_INCREMENT,
					Name VARCHAR(50) NULL,
					PRIMARY KEY (id))
			`)
	if err != nil {
		return fmt.Errorf("failed to create Interests table: %v", err)
	}

	_, err = db.Exec(
		`CREATE TABLE Person_has_Interests (
				p_id INT UNSIGNED NOT NULL,
				i_id INT UNSIGNED NOT NULL,
				PRIMARY KEY (p_id, i_id),
				INDEX fk_Person_has_Interests_Interests1_idx (i_id ASC),
				INDEX fk_Person_has_Interests_Person1_idx (p_id ASC),
				CONSTRAINT fk_Person_has_Interests_Person1
						FOREIGN KEY (p_id)
						REFERENCES Person (id)
						ON DELETE CASCADE
						ON UPDATE NO ACTION,
				CONSTRAINT fk_Person_has_Interests_Interests1
						FOREIGN KEY (i_id)
						REFERENCES Interests (id)
						ON DELETE CASCADE
						ON UPDATE NO ACTION)
		`)
	if err != nil {
		return fmt.Errorf("failed to create Person_has_Interests table: %v", err)
	}
	return nil
}

const fillPersonQueryTemplate = "INSERT INTO Person (id, FirstName, LastName, ProfileImageURL) VALUES (?, ?, ?, ?)"

func insertPeople(conn *sql.DB, people int) error {
	stmt, err := conn.Prepare(fillPersonQueryTemplate)
	if err != nil {
		log.Printf("error when preparing to fill people: %v\n", err)
		return err
	}
	for i := 1; i <= people; i++ {
		fn := "Joe" + strconv.Itoa(i)
		ln := "Schmoe" + strconv.Itoa(i)
		pp := "https://placekitten.com/200/300"
		_, err = stmt.Exec(i, fn, ln, pp)
		if err != nil {
			log.Printf("error when inserting a Person: %v\n", err)
			return err
		}
	}
	return nil
}

func insertPeopleArgs(people int) [][]interface{} {
	var allArgs [][]interface{}
	for i := 1; i <= people; i++ {
		fn := "Jane" + strconv.Itoa(i)
		ln := "Schmoe" + strconv.Itoa(i)
		pp := "https://placekitten.com/200/300"
		args := []interface{}{fn, ln, pp}
		allArgs = append(allArgs, args)
	}
	return allArgs
}

const insFriendQueryTemplate = "INSERT INTO Friends (p_id1, p_id2) VALUES (?, ?)"

func insertFriends(conn *sql.DB, people, avgFriends int) error {
	// Use a 60% range centered on the average.
	friendRange := avgFriends * 6 / 10
	// Prepare the offset by scooting the 60% range back half way.
	frOffset := avgFriends - friendRange/2
	for i := 1; i <= people; i++ {
		numFriends := rand.Intn(friendRange) + frOffset
		// Create a list of all possible friends.
		pfs := possibleSlice(people, i)
		// Remove self.
		pfs = deleteInd(pfs, i-1)
		for j := 1; j <= numFriends; j++ {
			// Select a random Friend index.
			fInd := rand.Intn(len(pfs) - 1)
			// Get the random Friend id.
			fID := pfs[fInd]
			_, err := conn.Exec(insFriendQueryTemplate, i, fID)
			if err != nil {
				log.Printf("error when inserting Friends: %v\n", err)
				return err
			}
			pfs = deleteInd(pfs, fInd)
		}
	}
	return nil
}

func possibleSlice(size, me int) []int {
	pfs := make([]int, size)
	for i := range pfs {
		pfs[i] = i + 1
	}
	return pfs
}

func deleteInd(pfs []int, i int) []int {
	pfs[len(pfs)-1], pfs[i] = pfs[i], pfs[len(pfs)-1]
	return pfs[:len(pfs)-1]
}

const insInterestQueryTemplate = "INSERT INTO Interests (id, Name) VALUES (?, ?)"

func insertInterests(conn *sql.DB, interests int) error {
	for i := 1; i <= interests; i++ {
		n := "Hobby " + strconv.Itoa(i)
		_, err := conn.Exec(insInterestQueryTemplate, i, n)
		if err != nil {
			log.Printf("error when inserting an Interest: %v\n", err)
			return err
		}
	}
	return nil
}

const insInterestLinksQueryTemplate = "INSERT INTO Person_has_Interests (p_id, i_id) VALUES (?, ?)"

func insertInterestLinks(conn *sql.DB, people, numInterests, avgInterests int) error {
	// Use a 10% range centered on the average.
	interRange := avgInterests * 6 / 10
	// Prepare the offset by scooting the 10% range back half way.
	irOffset := avgInterests - interRange/2
	for i := 1; i <= people; i++ {
		myInterCount := rand.Intn(interRange) + irOffset
		// Create a list of all possible interests.
		pis := possibleSlice(people, i)
		for j := 1; j <= myInterCount; j++ {
			// Select a random interest index.
			iInd := rand.Intn(len(pis) - 1)
			// Get the random interest id.
			iID := pis[iInd]
			_, err := conn.Exec(insInterestLinksQueryTemplate, i, iID)
			if err != nil {
				log.Printf("error when inserting an Interest Link: %v\n", err)
				return err
			}
			pis = deleteInd(pis, iInd)
		}
	}
	return nil
}
