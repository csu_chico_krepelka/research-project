// Package main implements a server for Greeter service.
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"

	"./databases/mysql"
	"./databases/query"

	"google.golang.org/grpc"

	pb "../../helper"
)

const port = ":8080"

type db interface {
	Connect() error
	Healthy() bool
	Setup() error
	TearDown()
	Close()
	Fill(people, avgFriends, avgInterests int) error
	CreateQuery(qNum int) (query.Query, error)
	//RunRetrieveQuery(qNum int) query.Query
	//RunUpdateQuery(qNum int) query.Query
	//RunDeleteQuery(qNum int) query.Query
}

var (
	conn db

	registeredTypes = map[string]func() db{
		"mysql": func() db { return db(mysql.NewConnection()) },
	}
)

type server struct {
	pb.UnimplementedTestHelperServer
}

// HealthCheck implements helper.HealthCheck
func (s *server) HealthCheck(ctx context.Context, in *pb.HealthCheckRequest) (*pb.HealthCheckResponse, error) {
	log.Println("Health Check")

	h := conn.Healthy()
	if !h {
		log.Println("database connection was not healthy")
	}

	//err := conn.Fill(10, 5, 3)
	//if err != nil {
	//	log.Println("failed to run fill query")
	//}

	q, err := conn.CreateQuery(1)
	if err != nil {
		log.Println("failed to create query: ", err)
	}
	err = q.Run()
	if err != nil {
		log.Println("failed to run query: ", err)
	}

	return &pb.HealthCheckResponse{Healthy: h}, nil
}

func main() {
	dbType := os.Getenv("DBTYPE")
	getConn, ok := registeredTypes[dbType]
	if !ok {
		log.Fatalf("unrecognized DBTYPE set: saw %q", dbType)
		return
	}
	conn = getConn()

	err := conn.Connect()
	if err != nil {
		log.Fatalf("failed to connect to database, not starting: %v", err)
		return
	}
	defer conn.Close()

	h := conn.Healthy()
	if !h {
		log.Fatalf("failed healthCheck to database, not starting")
		return
	}

	conn.TearDown()
	err = conn.Setup()
	if err != nil {
		log.Fatalf("failed to setup database, not starting: %v", err)
		return
	}
	//defer conn.TearDown()

	fmt.Println("Serving...")
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterTestHelperServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
