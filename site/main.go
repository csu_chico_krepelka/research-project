// Package main implements a client for the TestHelper service.
package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"../helper/go/client"
)

const (
	listeningPort = "8080"
)

type page struct {
	Title string
	Body  []byte
}

func loadPage(title string) *page {
	filename := title + ".html"
	body, _ := ioutil.ReadFile(filename)
	return &page{Title: title, Body: body}
}

func indexHandler(w http.ResponseWriter, _ *http.Request) {
	p := loadPage("index")
	fmt.Fprintf(w, "%s", p.Body)
}

type serverInfo struct {
	IP string
}

func healthCheckHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(w, "could not decode request body %v: %v", r.Body, err)
		return
	}

	ip := r.Form.Get("IP")

	ctx, cancel := context.WithTimeout(r.Context(), time.Second*4)
	fmt.Println("Healthcheck")
	h, err := client.HealthCheck(ctx, ip)
	fmt.Println("x Healthchecked")
	if err != nil {
		cancel()
		fmt.Fprintf(w, "could not health check %q: %v", ip, err)
		return
	}

	s := "HEALTHY"
	if !h {
		s = "UN" + s
	}
	fmt.Fprintf(w, "Server at %q was %s", ip, s)
	cancel()
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/health", healthCheckHandler)
	err := http.ListenAndServe(":"+listeningPort, nil)
	log.Fatalf("%v", err)
}
